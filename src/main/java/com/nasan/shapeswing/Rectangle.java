/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeswing;

/**
 *
 * @author nasan
 */
public class Rectangle extends Shape{
    private double width;
    private double height;
    
    public Rectangle(double width,double height){
        super("Rectangle");
        this.height = height;
        this.width = width;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeight(double height) {
        this.height = height;
    }
    
    
    @Override
    public double calArea() {
        return width*height;
    }

    @Override
    public double calPerimeter() {
        return (width*2)+(height*2);
    }
    
}
