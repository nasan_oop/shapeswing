/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author nasan
 */
public class TriangleFrame extends JFrame{

    JLabel lblbase;
    JLabel lblHeight;
    JTextField txtBase;
    JTextField txtHeight;
    JButton btnCalculate;
    JLabel lblResult;

    public TriangleFrame() {
        super("Triangle");
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblbase = new JLabel("Base:", JLabel.TRAILING);
        lblbase.setSize(50, 20);
        lblbase.setLocation(5, 5);
        lblbase.setBackground(Color.white);
        lblbase.setOpaque(true);
        this.add(lblbase);

        lblHeight = new JLabel("Height:", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(5, 30);
        lblHeight.setBackground(Color.white);
        lblHeight.setOpaque(true);
        this.add(lblHeight);

        txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);
        this.add(txtBase);

        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 30);
        this.add(txtHeight);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 20);
        this.add(btnCalculate);

        lblResult = new JLabel("Triangle base = ??? height = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText();
                    String strHeight = txtHeight.getText();
                    double base = Double.parseDouble(strBase);
                    double height = Double.parseDouble(strHeight);
                    Triangle triangle = new Triangle(base,height);
                    lblResult.setText("base = " + String.format("%.2f", triangle.getBase())
                            + " height = " + String.format("%.2f", triangle.getHeight())
                            + " area = " + String.format("%.2f", triangle.calArea())
                            + " perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception E) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtHeight.setText("");
                    txtBase.requestFocus();
                }
            }

        });
    }

    public static void main(String[] args) {
        TriangleFrame triangleFrame = new TriangleFrame();
        triangleFrame.setVisible(true);
    }    
}
