/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeswing;

/**
 *
 * @author nasan
 */
public class Triangle extends Shape{
    
    private double base;
    private double height;

    public Triangle(double base, double height) {
        super("Triangle");
        this.base = base;
        this.height = height;
    }

    public double getBase() {
        return base;
    }

    public double getHeight() {
        return height;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    
    @Override
    public double calArea() {
        return (base*height)/2;
    }

    @Override
    public double calPerimeter() {
        double c2,c;
        c2 = Math.pow(base, 2)+Math.pow(height, 2);
        c = Math.sqrt(c2);
        return base+height+c;
    }
    
}
