/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author nasan
 */
public class RectangleFrame extends JFrame {

    JLabel lblwidth;
    JLabel lblHeight;
    JTextField txtWidth;
    JTextField txtHeight;
    JButton btnCalculate;
    JLabel lblResult;

    public RectangleFrame() {
        super("Rectangle");
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblwidth = new JLabel("Width:", JLabel.TRAILING);
        lblwidth.setSize(50, 20);
        lblwidth.setLocation(5, 5);
        lblwidth.setBackground(Color.white);
        lblwidth.setOpaque(true);
        this.add(lblwidth);

        lblHeight = new JLabel("Height:", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(5, 30);
        lblHeight.setBackground(Color.white);
        lblHeight.setOpaque(true);
        this.add(lblHeight);

        txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 5);
        this.add(txtWidth);

        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 30);
        this.add(txtHeight);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 20);
        this.add(btnCalculate);

        lblResult = new JLabel("Rectangle width = ??? height = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidth = txtWidth.getText();
                    String strHeight = txtHeight.getText();
                    double width = Double.parseDouble(strWidth);
                    double height = Double.parseDouble(strHeight);
                    Rectangle rectangle = new Rectangle(width,height);
                    lblResult.setText("width = " + String.format("%.2f", rectangle.getWidth())
                            + " height = " + String.format("%.2f", rectangle.getHeight())
                            + " area = " + String.format("%.2f", rectangle.calArea())
                            + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception E) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtHeight.setText("");
                    txtWidth.requestFocus();
                }
            }

        });
    }

    public static void main(String[] args) {
        RectangleFrame rectangleFrame = new RectangleFrame();
        rectangleFrame.setVisible(true);
    }
}
